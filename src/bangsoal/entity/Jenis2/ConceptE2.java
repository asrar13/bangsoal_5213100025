/*
 * The MIT License
 *
 * Copyright 2015 Windows 8.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package bangsoal.entity.Jenis2;

import java.io.Serializable;

/**
 *
 * @author Windows 8
 */

/* 
Melakukan refactor pada bangsoal entity concept
Karena apabila ada kelas lain yang memiliki kegunaan untuk hal berbeda tetapi memiliki method yang sama dengan Concept
di package bangoal.entity, agar tidak menimpa method itu dan tidak repot-repot membuat method yang sama. 
*/

public interface ConceptE2 extends Serializable {

    boolean equals(Object obj);
    
}
